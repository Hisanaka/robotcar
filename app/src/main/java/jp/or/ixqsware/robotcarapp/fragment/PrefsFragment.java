package jp.or.ixqsware.robotcarapp.fragment;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import java.util.ArrayList;
import java.util.Objects;
import jp.or.ixqsware.robotcarapp.R;
import jp.or.ixqsware.robotcarapp.parts.ActionInfo;

import static jp.or.ixqsware.robotcarapp.Constants.ARG_SECTION_NUMBER;
import static jp.or.ixqsware.robotcarapp.Constants.GO_FORWARD;
import static jp.or.ixqsware.robotcarapp.Constants.LEFT_OUTPUT_VALUE;
import static jp.or.ixqsware.robotcarapp.Constants.MODE_ON;
import static jp.or.ixqsware.robotcarapp.Constants.OUTPUT_VALUE;
import static jp.or.ixqsware.robotcarapp.Constants.PREFS_NAME;
import static jp.or.ixqsware.robotcarapp.Constants.RIGHT_OUTPUT_VALUE;
import static jp.or.ixqsware.robotcarapp.Constants.STOP_MOVE;
import static jp.or.ixqsware.robotcarapp.Constants.TIME_VALUE;
import static jp.or.ixqsware.robotcarapp.Constants.TURN_LEFT;
import static jp.or.ixqsware.robotcarapp.Constants.TURN_RIGHT;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_ACTION_CHARACTERISTIC;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_LENGTH_CHARACTERISTIC;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_MODE_CHARACTERISTIC;

/**
 * 設定用フラグメント
 */
public class PrefsFragment extends PreferenceFragmentCompat {
    protected FragmentCallbackListener mListener;

    EditTextPreference editOutput;
    EditTextPreference editRightOutput;
    EditTextPreference editLeftOutput;
    EditTextPreference editTime;

    public interface FragmentCallbackListener {
        void connectToDevice(BluetoothDevice device);
        void writeCharacteristic(String uuid, int mValue);
        void writeCharacteristic(String uuid, ArrayList<Integer> arrValues);
    }

    public static PrefsFragment newInstance(int sectionNumber) {
        PrefsFragment instance = new PrefsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCallbackListener) {
            mListener = (FragmentCallbackListener) context;
        }
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String key) {
        setPreferencesFromResource(R.xml.preference, key);

        final SharedPreferences preferences
                = Objects.requireNonNull(getContext()).getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        editOutput = (EditTextPreference) getPreferenceScreen().findPreference(OUTPUT_VALUE);
        editRightOutput = (EditTextPreference) getPreferenceScreen().findPreference(RIGHT_OUTPUT_VALUE);
        editLeftOutput = (EditTextPreference) getPreferenceScreen().findPreference(LEFT_OUTPUT_VALUE);
        editTime = (EditTextPreference) getPreferenceScreen().findPreference(TIME_VALUE);

        editOutput.setText(String.valueOf(preferences.getInt(OUTPUT_VALUE, 50)));
        editRightOutput.setText(String.valueOf(preferences.getInt(RIGHT_OUTPUT_VALUE, 50)));
        editLeftOutput.setText(String.valueOf(preferences.getInt(LEFT_OUTPUT_VALUE, 50)));
        editTime.setText(String.valueOf(preferences.getInt(TIME_VALUE, 1000)));

        Preference buttonCheck = getPreferenceScreen().findPreference("check_button");
        buttonCheck.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                savePreferences(preferences);
                int outputValue = preferences.getInt(OUTPUT_VALUE, 50);
                int rightValue = preferences.getInt(RIGHT_OUTPUT_VALUE, 50);
                int leftValue = preferences.getInt(LEFT_OUTPUT_VALUE, 50);
                int timeValue = preferences.getInt(TIME_VALUE, 1000);

                mListener.writeCharacteristic(UUID_LENGTH_CHARACTERISTIC, 20);
                for (int i = 0; i < 20; i++) {
                    ActionInfo actionInfo = new ActionInfo();
                    switch (i) {
                        case 0:
                        case 3:
                        case 6:
                        case 10:
                        case 13:
                        case 16:
                            actionInfo.setAction(GO_FORWARD);
                            actionInfo.setPower1(outputValue);
                            actionInfo.setPower2(outputValue);
                            actionInfo.setTime(timeValue);
                            break;

                        case 2:
                        case 12:
                            actionInfo.setAction(TURN_RIGHT);
                            actionInfo.setPower1(rightValue);
                            actionInfo.setPower2(rightValue);
                            actionInfo.setTime(timeValue);
                            break;

                        case 5:
                        case 15:
                            actionInfo.setAction(TURN_LEFT);
                            actionInfo.setPower1(leftValue);
                            actionInfo.setPower2(leftValue);
                            actionInfo.setTime(timeValue);
                            break;

                        case 8:
                        case 18:
                            actionInfo.setAction(TURN_RIGHT);
                            actionInfo.setPower1(rightValue);
                            actionInfo.setPower2(rightValue);
                            actionInfo.setTime(timeValue * 2);
                            break;

                        default:
                            actionInfo.setAction(STOP_MOVE);
                            actionInfo.setPower1(0);
                            actionInfo.setPower2(0);
                            actionInfo.setTime(timeValue);
                            break;
                    }
                    mListener.writeCharacteristic(UUID_ACTION_CHARACTERISTIC,
                            actionInfo.getActionInfo());
                }
                mListener.writeCharacteristic(UUID_MODE_CHARACTERISTIC, MODE_ON);
                return true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        SharedPreferences preferences
                = Objects.requireNonNull(getContext()).getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        savePreferences(preferences);
        super.onPause();
    }

    private void savePreferences(SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(OUTPUT_VALUE, Integer.valueOf(editOutput.getText()));
        editor.putInt(RIGHT_OUTPUT_VALUE, Integer.valueOf(editRightOutput.getText()));
        editor.putInt(LEFT_OUTPUT_VALUE, Integer.valueOf(editLeftOutput.getText()));
        editor.putInt(TIME_VALUE, Integer.valueOf(editTime.getText()));
        editor.apply();
    }
}
