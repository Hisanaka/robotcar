package jp.or.ixqsware.robotcarapp;

/**
 * 定数
 * Created by hnakadate on 15/09/14.
 */
public class Constants {
    public static final String TAG = "RobotCar ";

    public static final String ARG_SECTION_NUMBER     = "section_number";
    static final String ARG_BLUETOOTH_DEVICE          = "bluetooth_device";
    public static final String ARG_PARCELABLE_OBJECTS = "parcelable_objects";

    public static final String ARG_TITLE   = "title";
    public static final String ARG_MESSAGE = "message";
    public static final String ARG_EXTRA   = "extra";

    static final int REQUEST_ENABLE_BLUETOOTH                  = 0;
    static final int REQUEST_LOCATION_PERMISSION               = 1;
    static final int REQUEST_LOCATION_MODE_ENABLE              = 2;
    static final int REQUEST_CAMERA_PERMISSION                 = 3;
    static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 4;

    static final String TAG_DETECT           = "detect";
    static final String TAG_CAMERA           = "camera";
    static final String TAG_MAP              = "map";
    static final String TAG_ANNOUNCE         = "announce";
    static final String TAG_PREFERENCES      = "preferences";
    static final String TAG_SAMPLE           = "sample";


    public static final String PREFS_NAME         = "DEVICE_PREFS";
    public static final String OUTPUT_VALUE       = "output_value";
    public static final String RIGHT_OUTPUT_VALUE = "right_output_value";
    public static final String LEFT_OUTPUT_VALUE  = "left_output_value";
    public static final String TIME_VALUE         = "time_value";

    static final int SECTION_DETECT             = 0;
    public static final int SECTION_MAP         = 1;
    public static final int SECTION_PREFERENCES = 2;
    static final int SECTION_CAMERA             = 3;
    static final int SECTION_SAMPLE             = 4;

    public static final int MODE_OFF = 0;
    public static final int MODE_ON  = 1;

    public static final int STOP_MOVE = 0;
    public static final int GO_FORWARD = 1;
    public static final int TURN_RIGHT = 2;
    public static final int TURN_LEFT  = 4;

    static final long SCAN_PERIOD = 5000;

    static final String UUID_DEVICE_INFORMATION_SERVICE
            = "0000180a-0000-1000-8000-00805f9b34fb";

    static final String UUID_FIRMWARE_VERSION
            = "00002a26-0000-1000-8000-00805f9b34fb";

    static final String UUID_RADIO_CONTROL_SERVICE
            = "4e76f100-4f13-4605-b976-f7cd9c14af82";

    public static final String UUID_MODE_CHARACTERISTIC
            = "4e76f101-4f13-4605-b976-f7cd9c14af82";

    public static final String UUID_ACTION_CHARACTERISTIC
            = "4e76f102-4f13-4605-b976-f7cd9c14af82";

    public static final String UUID_LENGTH_CHARACTERISTIC
            = "4e76f103-4f13-4605-b976-f7cd9c14af82";

    public static final String UUID_NOTIFY_CHARACTERISTIC
            = "4e76f204-4f13-4605-b976-f7cd9c14af82";
}
