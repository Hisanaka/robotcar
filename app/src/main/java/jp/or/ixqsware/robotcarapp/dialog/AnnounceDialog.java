package jp.or.ixqsware.robotcarapp.dialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import jp.or.ixqsware.robotcarapp.R;

import static jp.or.ixqsware.robotcarapp.Constants.ARG_EXTRA;
import static jp.or.ixqsware.robotcarapp.Constants.ARG_MESSAGE;
import static jp.or.ixqsware.robotcarapp.Constants.ARG_TITLE;

/**
 * 各種案内用ダイアログ
 *
 * Created by hnakadate on 15/12/22.
 */
public class AnnounceDialog extends DialogFragment {

    public static AnnounceDialog newInstance(Fragment target, int requestCode,
                                             String title, String message) {
        AnnounceDialog instance = new AnnounceDialog();
        instance.setTargetFragment(target, requestCode);
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        instance.setArguments(args);
        return instance;
    }

    public AnnounceDialog() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_announe, container, false);

        Bundle args = getArguments();
        getDialog().setTitle(args.getString(ARG_TITLE));
        getDialog().setCancelable(true);
        getDialog().setCanceledOnTouchOutside(true);

        String message = args.getString(ARG_MESSAGE);
        final String extraData = args.getString(ARG_EXTRA);

        TextView messageView = rootView.findViewById(R.id.message_view);
        messageView.setText(message);

        Button okButton = rootView.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment target = getTargetFragment();
                if (target != null) {
                    Intent data = new Intent();
                    data.putExtra(ARG_EXTRA, extraData);
                    target.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
                }
                dismiss();
            }
        });

        return rootView;
    }
}
