package jp.or.ixqsware.robotcarapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import jp.or.ixqsware.robotcarapp.dialog.AnnounceDialog;
import jp.or.ixqsware.robotcarapp.fragment.BaseFragment;
import jp.or.ixqsware.robotcarapp.fragment.CameraFragment;
import jp.or.ixqsware.robotcarapp.fragment.DetectFragment;
import jp.or.ixqsware.robotcarapp.fragment.MapFragment;
import jp.or.ixqsware.robotcarapp.fragment.PrefsFragment;

import static jp.or.ixqsware.robotcarapp.Constants.ARG_BLUETOOTH_DEVICE;
import static jp.or.ixqsware.robotcarapp.Constants.ARG_PARCELABLE_OBJECTS;
import static jp.or.ixqsware.robotcarapp.Constants.ARG_SECTION_NUMBER;
import static jp.or.ixqsware.robotcarapp.Constants.REQUEST_CAMERA_PERMISSION;
import static jp.or.ixqsware.robotcarapp.Constants.REQUEST_ENABLE_BLUETOOTH;
import static jp.or.ixqsware.robotcarapp.Constants.REQUEST_LOCATION_MODE_ENABLE;
import static jp.or.ixqsware.robotcarapp.Constants.REQUEST_LOCATION_PERMISSION;
import static jp.or.ixqsware.robotcarapp.Constants.REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION;
import static jp.or.ixqsware.robotcarapp.Constants.SCAN_PERIOD;
import static jp.or.ixqsware.robotcarapp.Constants.SECTION_CAMERA;
import static jp.or.ixqsware.robotcarapp.Constants.SECTION_DETECT;
import static jp.or.ixqsware.robotcarapp.Constants.SECTION_MAP;
import static jp.or.ixqsware.robotcarapp.Constants.SECTION_PREFERENCES;
import static jp.or.ixqsware.robotcarapp.Constants.TAG;
import static jp.or.ixqsware.robotcarapp.Constants.TAG_ANNOUNCE;
import static jp.or.ixqsware.robotcarapp.Constants.TAG_CAMERA;
import static jp.or.ixqsware.robotcarapp.Constants.TAG_DETECT;
import static jp.or.ixqsware.robotcarapp.Constants.TAG_MAP;
import static jp.or.ixqsware.robotcarapp.Constants.TAG_PREFERENCES;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_DEVICE_INFORMATION_SERVICE;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_FIRMWARE_VERSION;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_RADIO_CONTROL_SERVICE;

public class MainActivity extends AppCompatActivity
            implements BaseFragment.FragmentCallbackListener, PrefsFragment.FragmentCallbackListener {
    private static final int WRITE_CHAR_INTERVAL_MILLIS = 150;

    private FrameLayout frameLayout;
    private int currentFragment = SECTION_DETECT;
    private ProgressBar progressBar = null;

    private ArrayList<BluetoothDevice> arrDevices = new ArrayList<>();
    private BluetoothGatt mBtGatt;
    private BluetoothLeScanner mBtLeScanner;
    private BluetoothDevice bleDevice;

    private ArrayList<? extends Parcelable> arrayList = null;

    private final ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            BluetoothDevice device = result.getDevice();
            if (!arrDevices.contains(device)) { arrDevices.add(device); }
            super.onScanResult(callbackType, result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> result) {
            super.onBatchScanResults(result);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    gatt.discoverServices();

                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Snackbar.make(
                            frameLayout,
                            getString(R.string.disconnect_label),
                            Snackbar.LENGTH_SHORT
                    ).show();
                    setTitle(getString(R.string.app_name));
                    showFragment(SECTION_DETECT, null);
                }
            } else {
                /* status 133対策(Documentに記載がなく、詳細不明/連続して接続すると発生) */
                Snackbar.make(frameLayout, R.string.congest_label, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok_label, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                // とりあえず何もしない(ユーザーからのレスポンス待ちで時間稼ぎ)
                                }
                        })
                        .setActionTextColor(
                                ContextCompat.getColor(getApplicationContext(), R.color.snackbar_action))
                        .show();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status != BluetoothGatt.GATT_SUCCESS){
                Snackbar.make(frameLayout, R.string.service_detection_fail, Snackbar.LENGTH_SHORT)
                        .show();
                setTitle(getString(R.string.app_name));

            } else {
                boolean isTargetDevice = false;
                for (BluetoothGattService service : gatt.getServices()) {
                    UUID uuid = service.getUuid();
                    if (uuid.equals(UUID.fromString(UUID_RADIO_CONTROL_SERVICE))) {
                        isTargetDevice = true;
                        break;
                    }
                }

                if (isTargetDevice) {
                    mBtGatt = gatt;
                    BluetoothGattCharacteristic charFirmwareVer
                            = mBtGatt.getService(UUID.fromString(UUID_DEVICE_INFORMATION_SERVICE))
                            .getCharacteristic(UUID.fromString(UUID_FIRMWARE_VERSION));
                    if (!mBtGatt.readCharacteristic(charFirmwareVer)) {
                        Snackbar.make(frameLayout, R.string.service_detection_fail, Snackbar.LENGTH_SHORT)
                                .show();
                    }
                } else {
                    Snackbar.make(frameLayout, getString(R.string.wrong_device), Snackbar.LENGTH_SHORT)
                            .show();
                }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status != BluetoothGatt.GATT_SUCCESS) {
                Snackbar.make(frameLayout, R.string.service_detection_fail, Snackbar.LENGTH_SHORT)
                        .show();
                setTitle(getString(R.string.app_name));
            } else {
                showFragment(SECTION_CAMERA, null);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status != BluetoothGatt.GATT_SUCCESS) {
                gatt.writeCharacteristic(characteristic);
            }
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bleDevice = null;
        if (savedInstanceState != null) {
            currentFragment = savedInstanceState.getInt(ARG_SECTION_NUMBER);
            bleDevice = savedInstanceState.getParcelable(ARG_BLUETOOTH_DEVICE);
        }

        frameLayout = findViewById(R.id.container);

        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        BluetoothManager mBtManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (mBtManager != null) {
            BluetoothAdapter mBtAdapter = mBtManager.getAdapter();
            if (mBtAdapter != null) {
                mBtLeScanner = mBtAdapter.getBluetoothLeScanner();
                if (!mBtAdapter.isEnabled()) {
                    Intent intentBtOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intentBtOn, REQUEST_ENABLE_BLUETOOTH);
                }
            } else {
                Snackbar.make(frameLayout, R.string.unsupported_bluetooth, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok_label, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        })
                        .show();
            }
        } else {
            Snackbar.make(frameLayout, R.string.unsupported_bluetooth, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok_label, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermissions();
        } else if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestWriteExternalStoragePermissions();
        }

        if (bleDevice != null) {
            connectToDevice(bleDevice);
        } else {
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(ARG_PARCELABLE_OBJECTS, arrayList);
            showFragment(currentFragment, intent);
        }
    }

    @Override
    protected void onStop() {
        bleDevice = null;
        if (mBtGatt != null) {
            mBtGatt.close();
            mBtGatt = null;
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(ARG_SECTION_NUMBER, currentFragment);
        outState.putParcelable(ARG_BLUETOOTH_DEVICE, bleDevice);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                BluetoothManager mBtManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                BluetoothAdapter mBtAdapter = mBtManager.getAdapter();
                mBtLeScanner = mBtAdapter.getBluetoothLeScanner();
                showFragment(currentFragment, null);
            } else {
                finish();
            }
        }
    }

    private void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResult) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResult[0] == PackageManager.PERMISSION_GRANTED) {
                    doScan();
                }
                break;
            case REQUEST_CAMERA_PERMISSION:
            case REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION:
                if (grantResult[0] == PackageManager.PERMISSION_GRANTED) {
                    switchFragment(SECTION_CAMERA);
                }
                break;
            default:
                return;
        }

        if (grantResult[0] != PackageManager.PERMISSION_GRANTED) {
            Snackbar.make(
                        frameLayout,
                        getString(R.string.permission),
                        Snackbar.LENGTH_LONG)
                    .setActionTextColor(getColor(R.color.snackbar_action))
                    .setAction(getString(R.string.retry_label), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestLocationPermissions();
                        }
                    })
                    .show();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestLocationPermissions() {
        requestPermissions(
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_LOCATION_PERMISSION
        );
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestCameraPermissions() {
        requestPermissions(
                new String[]{Manifest.permission.CAMERA},
                REQUEST_CAMERA_PERMISSION
        );
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestWriteExternalStoragePermissions() {
        requestPermissions(
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION
        );
    }

    @Override
    public void scanDevices() {
        if (mBtLeScanner == null) { return; }
        if (!isLocationModeEnabled()) {
            AnnounceDialog dialog = AnnounceDialog.newInstance(null,
                    REQUEST_LOCATION_MODE_ENABLE, getString(R.string.enable_location_title),
                    getString(R.string.enable_location));
            dialog.show(getSupportFragmentManager(), TAG_ANNOUNCE);

        }else if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermissions();

        } else {
            doScan();
        }
    }

    private boolean isLocationModeEnabled() {
        LocationManager locationManager
                = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null
                && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void doScan() {
        progressBar.setVisibility(View.VISIBLE);

        Handler stopHandler = new Handler();
        stopHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBtLeScanner.stopScan(leScanCallback);
                /* スキャンに失敗した場合にペアリング済みのデバイス一覧を表示する
                if (arrDevices.size() == 0) {
                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    Set<BluetoothDevice> deviceSet = bluetoothAdapter.getBondedDevices();
                    arrDevices = new ArrayList<>(deviceSet);
                }*/

                FragmentManager fragmentManager = getSupportFragmentManager();
                DetectFragment fragment
                        = (DetectFragment) fragmentManager.findFragmentByTag(TAG_DETECT);
                if (fragment != null && fragment.isVisible()) {
                    fragment.updateLists(arrDevices);
                }
                if (progressBar != null && progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }, SCAN_PERIOD);

        mBtLeScanner.stopScan(leScanCallback);
        mBtLeScanner.startScan(leScanCallback);
    }

    @Override
    public void connectToDevice(BluetoothDevice device) {
        setTitle(device.getName());
        bleDevice = device;
        mBtGatt = device.connectGatt(this, false, mGattCallback);
    }

    @Override
    public void disconnectDevice() {
        mBtGatt.disconnect();
    }

    @Override
    public void writeCharacteristic(String uuid_, int mValue) {
        if (mBtGatt == null) { return; }
        BluetoothGattService mBtGattService
                = mBtGatt.getService(UUID.fromString(UUID_RADIO_CONTROL_SERVICE));
        if (mBtGattService == null) {
            Snackbar.make(
                    frameLayout,
                    getString(R.string.not_found, getString(R.string.service)),
                    Snackbar.LENGTH_SHORT
            ).show();
            return;
        }

        UUID uuid = UUID.fromString(uuid_);
        BluetoothGattCharacteristic mBtChar = mBtGattService.getCharacteristic(uuid);
        if (mBtChar == null) {
            Snackbar.make(
                    frameLayout,
                    getString(R.string.not_found, getString(R.string.characteristic)),
                    Snackbar.LENGTH_SHORT
            ).show();
            return;
        }

        byte[] byteData = integerToBytes(mValue);
        //showWriteValue(byteData);
        mBtChar.setValue(byteData);
        mBtChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mBtGatt.writeCharacteristic(mBtChar);

        try {
            Thread.sleep(WRITE_CHAR_INTERVAL_MILLIS);
        } catch (InterruptedException ignored) {
        }
    }

    @Override
    public void writeCharacteristic(String uuid_, ArrayList<Integer> arrValues) {
        if (mBtGatt == null) { return; }

        byte[] byteData = new byte[arrValues.size() * 4];
        for (int i = 0; i < arrValues.size(); i++) {
            byte[] bytes = integerToBytes(arrValues.get(i));
            System.arraycopy(bytes, 0, byteData, i * 4, bytes.length);
        }
        BluetoothGattService mBtGattService
                = mBtGatt.getService(UUID.fromString(UUID_RADIO_CONTROL_SERVICE));
        if (mBtGattService == null) {
            Snackbar.make(
                    frameLayout,
                    getString(R.string.not_found, getString(R.string.service)),
                    Snackbar.LENGTH_SHORT
            ).show();
            return;
        }

        UUID uuid = UUID.fromString(uuid_);
        BluetoothGattCharacteristic mBtChar = mBtGattService.getCharacteristic(uuid);
        if (mBtChar == null) {
            Snackbar.make(
                    frameLayout,
                    getString(R.string.not_found, getString(R.string.characteristic)),
                    Snackbar.LENGTH_SHORT
            ).show();
            return;
        }
        //showWriteValue(byteData);

        mBtChar.setValue(byteData);
        mBtChar.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mBtGatt.writeCharacteristic(mBtChar);

        try {
            Thread.sleep(WRITE_CHAR_INTERVAL_MILLIS);
        } catch (InterruptedException ignored) {
        }
    }

    @Override
    public void switchFragment(int sectionNumber) {
        showFragment(sectionNumber, null);
    }

    @Override
    public void transferData(int sectionNumber, ArrayList<? extends Parcelable> objects) {
        arrayList = objects;
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(ARG_PARCELABLE_OBJECTS, objects);
        showFragment(sectionNumber, intent);
    }

    private void showFragment(int sectionNumber, Intent intent) {
        switch (sectionNumber) {
            case SECTION_DETECT:
                setTitle(getString(R.string.app_name));
                showDetectFragment();
                break;

            case SECTION_CAMERA:
                showCameraFragment();
                break;

            case SECTION_MAP:
                showMapFragment(intent);
                break;

            case SECTION_PREFERENCES:
                showPrefsFragment();
                break;
        }
    }

    private void setFragment(Fragment fragment, String tag, boolean addStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment, tag);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (addStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    private void showDetectFragment() {
        currentFragment = SECTION_DETECT;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        DetectFragment fragment = DetectFragment.newInstance(currentFragment);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, currentFragment);
        fragment.setArguments(args);

        setFragment(fragment, TAG_DETECT, true);
    }

    private void showMapFragment(Intent intent) {
        currentFragment = SECTION_MAP;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        MapFragment fragment = MapFragment.newInstance(currentFragment, intent);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, currentFragment);
        args.putParcelableArrayList(ARG_PARCELABLE_OBJECTS,
                intent.getParcelableArrayListExtra(ARG_PARCELABLE_OBJECTS));
        fragment.setArguments(args);

        setFragment(fragment, TAG_MAP, true);
    }

    private void showPrefsFragment() {
        currentFragment = SECTION_PREFERENCES;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        PrefsFragment fragment = PrefsFragment.newInstance(currentFragment);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, currentFragment);
        fragment.setArguments(args);

        setFragment(fragment, TAG_PREFERENCES, true);
    }

    private void showCameraFragment() {
        currentFragment = SECTION_CAMERA;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        CameraFragment fragment = CameraFragment.newInstance(currentFragment);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, currentFragment);
        fragment.setArguments(args);

        setFragment(fragment, TAG_CAMERA, true);
    }

    private byte[] integerToBytes(int i) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.putInt(i);
        return byteBuffer.array();
    }

    private void showWriteValue(byte[] byteData) {
        StringBuilder sb = new StringBuilder();
        for (byte b : byteData) {
            sb.append(String.format("%02X", b));
            sb.append("," );
        }
        Log.d(TAG, sb.toString().substring(0, sb.toString().length() - 1));
    }
}