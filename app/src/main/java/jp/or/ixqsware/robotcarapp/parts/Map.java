package jp.or.ixqsware.robotcarapp.parts;

import java.util.ArrayList;

import static jp.or.ixqsware.robotcarapp.Constants.GO_FORWARD;
import static jp.or.ixqsware.robotcarapp.Constants.TURN_LEFT;
import static jp.or.ixqsware.robotcarapp.Constants.TURN_RIGHT;

/**
 * マップの状態を保持する
 *
 * Created by hnakadate on 2017/06/04.
 */

public class Map {
    private int ROWS;
    private int COLS;

    private int top;
    private int left;
    private int width;
    private int height;

    private ArrayList<CellAddress> arrAddresses = new ArrayList<>();

    private CellState cells[][];

    public Map() {
        this.ROWS = 5;
        this.COLS = 5;
        cells = new CellState[ROWS][COLS];
        for (int r = 0; r < ROWS; r++) {
            for (int c = 0; c < COLS; c++) {
                cells[r][c] = new CellState();
                cells[r][c].setCellStatus(CellState.CELL_STATUS.OFF);
            }
        }
    }

    public Map(int rows, int cols) {
        this.ROWS = rows;
        this.COLS = cols;
        cells = new CellState[ROWS][COLS];
        for (int r = 0; r < this.ROWS; r++) {
            for (int c = 0; c < this.COLS; c++) {
                cells[r][c] = new CellState();
                cells[r][c].setCellStatus(CellState.CELL_STATUS.OFF);
            }
        }
    }

    public int getRowsCount() {
        return this.ROWS;
    }

    public int getColumnsCount() {
        return this.COLS;
    }

    public void setTop(int top_) {
        this.top = top_;
    }

    public int getTop() {
        return this.top;
    }

    public void setLeft(int left_) {
        this.left = left_;
    }

    public int getLeft() {
        return this.left;
    }

    public void setWidth(int width_) {
        this.width = width_;
        float cellWidth = getCellWidth();
        for (int r = 0; r < this.ROWS; r++) {
            for (int c = 0; c < this.COLS; c++) {
                cells[r][c].setWidth(cellWidth);
                cells[r][c].setLeft((int)cellWidth * c);
            }
        }
    }

    public int getWidth() {
        return this.width;
    }

    public void setHeight(int height_) {
        this.height = height_;
        float cellHeight = getCellHeight();
        for (int r = 0; r < this.ROWS; r++) {
            for (int c = 0; c < this.COLS; c++) {
                cells[r][c].setHeight(cellHeight);
                cells[r][c].setTop((int)cellHeight * r);
            }
        }
    }

    public int getHeight() {
        return this.height;
    }

    public CellState[][] getCells() {
        return cells;
    }

    public String getObject(int r, int c) {
        return cells[r][c].getObject();
    }

    public void toggleCellStatus(int r, int c, CellState.CELL_STATUS status_) {
        CellState cellState = cells[r][c];
        cellState.setCellStatus(status_);

        if (status_ == CellState.CELL_STATUS.ON) {
            CellAddress cellAddress = new CellAddress();
            cellAddress.setAddress(r, c);
            arrAddresses.add(cellAddress);

        } else if (arrAddresses.size() > 0) {
            arrAddresses.remove(arrAddresses.size() - 1);

        } else {
            arrAddresses.clear();
            CellAddress cellAddress = new CellAddress();
            cellAddress.setAddress(r, c);
            arrAddresses.add(cellAddress);
        }
    }

    public int getCellWidth() {
        //return this.width <= this.height ? this.width / COLS : this.height / ROWS;
        return this.width / this.COLS;
    }

    public int getCellHeight() {
        return this.height / this.ROWS;
    }

    public int[] getNextToLastCellAddress() {
        if (arrAddresses.size() > 1) {
            return arrAddresses.get(arrAddresses.size() - 2).getAddress();
        } else {
            return new int[]{-1, -1};
        }
    }

    public boolean isSelected(int row, int col) {
        // 戻る場合を考慮してarrAddressの最終セルかどうかの判定を加える
        return cells[row][col].getCellStatus() == CellState.CELL_STATUS.ON
                && arrAddresses.get(arrAddresses.size() - 1).getAddress()[0] != row
                && arrAddresses.get(arrAddresses.size() - 1).getAddress()[1] != col;
    }

    public ArrayList<CellAddress> getRoute() {
        return this.arrAddresses;
    }

    public ArrayList<Integer> getDirections() {
        ArrayList<Integer> arrDirections = new ArrayList<>();
        arrDirections.add(GO_FORWARD);

        for (int i = 2; i < arrAddresses.size(); i++) {
            int[] pos1 = arrAddresses.get(i - 2).getAddress();
            int[] pos2 = arrAddresses.get(i - 1).getAddress();
            int[] pos3 = arrAddresses.get(i).getAddress();

            CellState cell1 = cells[pos1[0]][pos1[1]];
            CellState cell2 = cells[pos2[0]][pos2[1]];
            CellState cell3 = cells[pos3[0]][pos3[1]];

            float dir = (cell1.getCentreX() - cell2.getCentreX()) * (cell3.getCentreY() - cell2.getCentreY())
                    - (cell1.getCentreY() - cell2.getCentreY()) * (cell3.getCentreX() - cell2.getCentreX());
            if (dir > 0) {
                arrDirections.add(TURN_LEFT);
            } else if (dir < 0) {
                arrDirections.add(TURN_RIGHT);
            }
            arrDirections.add(GO_FORWARD);
        }
        return arrDirections;
    }

    public void resetRoute() {
        arrAddresses.clear();
    }
}
