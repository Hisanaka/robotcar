package jp.or.ixqsware.robotcarapp.parts;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import jp.or.ixqsware.robotcarapp.R;

public class ImageRecognition {
    private static ImageRecognition instance = new ImageRecognition();
    private static Context context;
    private static Bitmap bmp = null;
    private static Bitmap output;
    private static MapInfo mapInfo;

    private static BaseLoaderCallback onLoaderCallback = new BaseLoaderCallback(context) {
        @Override
        public void onManagerConnected(int status) {
            super.onManagerConnected(status);
            if (bmp == null) {
                return;
            }

            // グレースケール化
            Mat gray = new Mat(bmp.getWidth(), bmp.getHeight(), CvType.CV_8UC1);
            Utils.bitmapToMat(bmp, gray);
            Imgproc.cvtColor(gray, gray, Imgproc.COLOR_BGR2GRAY);

            // 二値化
            Imgproc.threshold(gray, gray, 0, 120,
                    Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);

            /* 四角形(枠線)の検出 */
            List<MatOfPoint> borderContours = new ArrayList<>();
            detectRectangle(gray, borderContours);

            /* 図形の検出 */
            List<ShapeInfo> shapeInfos = new ArrayList<>();
            detectShapes(gray, shapeInfos);
            updateCellNumberOfShape(borderContours, shapeInfos);

            // MapInfoオプジェクト作成
            int cntRows = countRows(borderContours);
            int cntCols = countColumns(borderContours);
            mapInfo.setRows(cntRows);
            mapInfo.setCols(cntCols);
            for (ShapeInfo info : shapeInfos) {
                mapInfo.addShapeInfo(info);
            }

            // 出力画像の作成
            Color color = Color.valueOf(ContextCompat.getColor(context, R.color.color_accent));
            Scalar drawColor = new Scalar(255 * color.red(), 255 * color.green(), 255 * color.blue());

            Mat temp = new Mat(bmp.getWidth(), bmp.getHeight(), CvType.CV_8UC3);
            Utils.bitmapToMat(bmp, temp);

            // 検出した四角形の描画
            for (int i = 0; i < borderContours.size(); i++) {
                Imgproc.drawContours(temp, borderContours, i, drawColor, 10);

                Point point = getLeftTopPoint(borderContours.get(i));
                Imgproc.putText(temp, Integer.toString(i), point,
                        Imgproc.FONT_HERSHEY_SIMPLEX, 2.2f,
                        new Scalar(255, 255, 255), 10);
            }

            // 検出した図形の描画
            for (int i = 0; i < shapeInfos.size(); i++) {
                ShapeInfo shapeInfo = shapeInfos.get(i);
                Imgproc.drawContours(temp, shapeInfo.getContours(), 0, drawColor, -1);
            }
            output = Bitmap.createBitmap(temp.width(), temp.height(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(temp, output);
        }
    };

    public ImageRecognition() {
        super();
    }

    public static ImageRecognition getInstance(Context context_) {
        context = context_;
        return instance;
    }

    public void recognitionImage(byte[] bytes) {
        mapInfo = new MapInfo();
        bmp = BitmapFactory.decodeByteArray(bytes,0, bytes.length);

        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, context, onLoaderCallback);
        } else {
            onLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void recognitionImage(Bitmap bmp_) {
        mapInfo = new MapInfo();
        bmp = bmp_;
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, context, onLoaderCallback);
        } else {
            onLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public Bitmap getImage() {
        return output;
    }

    public MapInfo getMapInfo() {
        return mapInfo;
    }

    /* 図形を検出
     * 出典 : https://stackoverrun.com/ja/q/9427488
     */
    private static void detectShapes(Mat mat, List<ShapeInfo> shapeInfos) {
        /* 面積の平方根 ÷ 周囲長 を定義 */
        // 星の場合は0.145±0.015
        double divMaxStar = 0.170;
        double divMinStar = 0.130;

        // 三角形の場合は0.21±0.015
        double divMaxTriangle = 0.21 + 0.015;
        double divMinTriangle = 0.21 - 0.015;

        // 円の場合は0.28±0.015
        double divMaxCircle = 0.28 + 0.015;
        double divMinCircle = 0.28 - 0.015;

        // 輪郭抽出
        List<MatOfPoint> tmp_contours = new ArrayList<>();
        Mat hierarchy = Mat.zeros(new Size(5, 5), CvType.CV_8UC1);
        Imgproc.findContours(mat, tmp_contours,
                hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        // サイズと形状でフィルタリング
        for (int i = 0; i < tmp_contours.size(); i++) {
            // 一定サイズ以下の領域は無視
            double cntArea = Imgproc.contourArea(tmp_contours.get(i));
            if (cntArea < 1000) {
                continue;
            }

            MatOfPoint2f ptmat2 = new MatOfPoint2f(tmp_contours.get(i).toArray());
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint approxf1 = new MatOfPoint();

            // 輪郭線の周囲長を取得
            double arcLength = Imgproc.arcLength(ptmat2, true);
            Imgproc.approxPolyDP(ptmat2, approx, 0.02 * arcLength, true);
            approx.convertTo(approxf1, CvType.CV_32S);

            ShapeInfo shapeInfo = new ShapeInfo();
            if (divMinStar < Math.sqrt(cntArea) / arcLength
                    && Math.sqrt(cntArea) / arcLength < divMaxStar) {
                shapeInfo.setType("star");
                shapeInfo.setContour(approxf1);
                shapeInfos.add(shapeInfo);

            } else if (divMinTriangle < Math.sqrt(cntArea) / arcLength
                    && Math.sqrt(cntArea) / arcLength < divMaxTriangle) {
                shapeInfo.setType("triangle");
                shapeInfo.setContour(approxf1);
                shapeInfos.add(shapeInfo);

            } else if (divMinCircle < Math.sqrt(cntArea) / arcLength
                    && Math.sqrt(cntArea) / arcLength < divMaxCircle) {
                shapeInfo.setType("circle");
                shapeInfo.setContour(approxf1);
                shapeInfos.add(shapeInfo);
            }
        }
    }

    /* 四角形を検出 */
    private static void detectRectangle(Mat mat, List<MatOfPoint> contours) {
        double divMaxSquare = 0.247 + 0.015;
        double divMinSqiare = 0.247 - 0.015;

        // 輪郭抽出
        List<MatOfPoint> tmp_contours = new ArrayList<>();
        Mat hierarchy = Mat.zeros(new Size(5, 5), CvType.CV_8UC1);
        Imgproc.findContours(mat, tmp_contours,
                hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_TC89_L1);

        // サイズと形状でフィルタリング
        for (int i = 0; i < tmp_contours.size(); i++) {
            // 一定サイズ以下の領域は無視
            double cntArea = Imgproc.contourArea(tmp_contours.get(i));
            if (cntArea < 3000) {
                continue;
            }

            MatOfPoint2f ptmat2 = new MatOfPoint2f(tmp_contours.get(i).toArray());
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint approxf1 = new MatOfPoint();

            // 輪郭線の周囲長を取得
            double arcLength = Imgproc.arcLength(ptmat2, true);
            Imgproc.approxPolyDP(ptmat2, approx, 0.02 * arcLength, true);
            approx.convertTo(approxf1, CvType.CV_32S);

            // 四角形以外は無視
            if (divMinSqiare < Math.sqrt(cntArea) / arcLength
                    && Math.sqrt(cntArea) / arcLength < divMaxSquare) {
                contours.add(approxf1);
            }
        }

        // 左上の座標でソート
        contours.sort(new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint o1, MatOfPoint o2) {
                Point p1 = getLeftTopPoint(o1);
                Point p2 = getLeftTopPoint(o2);
                double dx = p1.x - p2.x;
                double dy = p1.y - p2.y;
                // TODO リファクタが必要
                if (dy < -100) {
                    if (dx < -100) {
                        return -1;
                    } else if (100 < dx) {
                        return 1;
                    } else {
                        return -1;
                    }
                } else if (100 < dy) {
                    if (dx < -100) {
                        return -1;
                    } else if (100 < dx) {
                        return 1;
                    } else {
                        return 1;
                    }
                } else {
                    if (dx < -100) {
                        return -1;
                    } else if (100 < dx) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        });
    }

    private static Point getLeftTopPoint(MatOfPoint points) {
        double px = -1;
        double py = -1;
        for (Point p : points.toList()) {
            if (p.x < px || px < 0) {
                px = p.x;
            }
            if (p.y < py || py < 0) {
                py = p.y;
            }
        }
        return new Point(px, py);
    }

    private static Point getRightBottomPoint(MatOfPoint points) {
        double px = -1;
        double py = -1;
        for (Point p : points.toList()) {
            if (p.x > px || px < 0) {
                px = p.x;
            }
            if (p.y > py || py < 0) {
                py = p.y;
            }
        }
        return new Point(px, py);
    }

    private static int countRows(List<MatOfPoint> contours) {
        int count = 0;
        double py = -1;
        for (int i = 0; i < contours.size(); i++) {
            Point p = getLeftTopPoint(contours.get(i));
            if (py < p.y) {
                py = p.y;
                count++;
            } else if (p.y < py -130){
                break;
            }
        }
        return count;
    }

    private static int countColumns(List<MatOfPoint> contours) {
        int count = 0;
        double px = -1;
        for (int i = 0; i < contours.size(); i++) {
            Point p = getLeftTopPoint(contours.get(i));
            if (i == 0 || p.x < px - 100 || px + 100 < p.x) {
                px = p.x;
                count++;
            }
        }
        return count;
    }

    private static void updateCellNumberOfShape(List<MatOfPoint> borders, List<ShapeInfo> shapes) {
        for (int i = 0; i < shapes.size(); i++) {
            ShapeInfo shape = shapes.get(i);
            List<MatOfPoint> contours = shape.getContours();
            Moments m = Imgproc.moments(contours.get(0));
            int cx = (int) (m.m10 / m.m00);
            int cy = (int) (m.m01 / m.m00);
            Point p = new Point(cx, cy);

            for (int j = 0; j < borders.size(); j++) {
                Point topLeft = getLeftTopPoint(borders.get(j));
                Point bottomRight = getRightBottomPoint(borders.get(j));
                Rect rect = new Rect(topLeft, bottomRight);
                if (rect.contains(p)) {
                    shapes.get(i).setCellNumber(j);
                    break;
                }
            }
        }
    }

}
