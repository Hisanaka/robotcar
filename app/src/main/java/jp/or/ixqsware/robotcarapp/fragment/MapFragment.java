package jp.or.ixqsware.robotcarapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Objects;

import jp.or.ixqsware.robotcarapp.R;
import jp.or.ixqsware.robotcarapp.parts.ActionInfo;
import jp.or.ixqsware.robotcarapp.parts.MapInfo;
import jp.or.ixqsware.robotcarapp.view.MapView;

import static jp.or.ixqsware.robotcarapp.Constants.ARG_PARCELABLE_OBJECTS;
import static jp.or.ixqsware.robotcarapp.Constants.ARG_SECTION_NUMBER;
import static jp.or.ixqsware.robotcarapp.Constants.GO_FORWARD;
import static jp.or.ixqsware.robotcarapp.Constants.LEFT_OUTPUT_VALUE;
import static jp.or.ixqsware.robotcarapp.Constants.MODE_ON;
import static jp.or.ixqsware.robotcarapp.Constants.OUTPUT_VALUE;
import static jp.or.ixqsware.robotcarapp.Constants.PREFS_NAME;
import static jp.or.ixqsware.robotcarapp.Constants.RIGHT_OUTPUT_VALUE;
import static jp.or.ixqsware.robotcarapp.Constants.SECTION_PREFERENCES;
import static jp.or.ixqsware.robotcarapp.Constants.TIME_VALUE;
import static jp.or.ixqsware.robotcarapp.Constants.TURN_LEFT;
import static jp.or.ixqsware.robotcarapp.Constants.TURN_RIGHT;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_ACTION_CHARACTERISTIC;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_LENGTH_CHARACTERISTIC;
import static jp.or.ixqsware.robotcarapp.Constants.UUID_MODE_CHARACTERISTIC;

/**
 * マップフラグメント
 *
 * Created by hnakadate on 2017/06/17.
 */

public class MapFragment extends BaseFragment implements MapView.ViewCallbackListener {
    private MapView mapView;
    private Button startButton;

    public static MapFragment newInstance(int sectionNumber, Intent intent) {
        MapFragment instance = new MapFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        if (intent != null) {
            args.putParcelableArrayList(ARG_PARCELABLE_OBJECTS,
                    intent.getParcelableArrayListExtra(ARG_PARCELABLE_OBJECTS));
        }
        instance.setArguments(args);
        return instance;
    }

    public MapFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        View contentView = rootView.findViewById(R.id.contents_view);

        ArrayList<MapInfo> mapInfos = null;
        if (getArguments() != null) {
            mapInfos = getArguments().getParcelableArrayList(ARG_PARCELABLE_OBJECTS);
        }
        if (mapInfos != null) {
            mapView = new MapView(getContext(), mapInfos.get(0));
        } else {
            mapView = new MapView(getContext());
        }
        mapView.setListener(this);

        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        marginLayoutParams.setMargins(10, 10, 10,10);
        ((FrameLayout) contentView).addView(mapView, marginLayoutParams);

        startButton = rootView.findViewById(R.id.start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    ArrayList<Integer> arrDirections = mapView.getDirection();
                    mListener.writeCharacteristic(UUID_LENGTH_CHARACTERISTIC, arrDirections.size());

                    SharedPreferences preferences
                            = Objects.requireNonNull(getContext()).getSharedPreferences(
                                    PREFS_NAME, Context.MODE_PRIVATE);
                    int outputValue = preferences.getInt(OUTPUT_VALUE, 50);
                    int rightValue = preferences.getInt(RIGHT_OUTPUT_VALUE, 50);
                    int leftValue = preferences.getInt(LEFT_OUTPUT_VALUE, 50);
                    int timeValue = preferences.getInt(TIME_VALUE, 1000);

                    for (Integer mDir : arrDirections) {
                        ActionInfo actionInfo = new ActionInfo();
                        actionInfo.setAction(mDir);
                        switch (mDir) {
                            case GO_FORWARD:
                                actionInfo.setPower1(outputValue);
                                actionInfo.setPower2(outputValue);
                                break;

                            case TURN_RIGHT:
                                actionInfo.setPower1(rightValue);
                                actionInfo.setPower2(rightValue);
                                break;

                            case TURN_LEFT:
                                actionInfo.setPower1(leftValue);
                                actionInfo.setPower2(leftValue);
                                break;
                        }
                        actionInfo.setTime(timeValue);
                        mListener.writeCharacteristic(UUID_ACTION_CHARACTERISTIC,
                                actionInfo.getActionInfo());
                    }
                    mListener.writeCharacteristic(UUID_MODE_CHARACTERISTIC, MODE_ON);
                }
            }
        });
        startButton.setEnabled(false);

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_prefs, menu);
        Drawable drawIcon = menu.findItem(R.id.action_prefs).getIcon();
        drawIcon = DrawableCompat.wrap(drawIcon);
        DrawableCompat.setTint(drawIcon,
                ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.white));
        menu.findItem(R.id.action_prefs).setIcon(drawIcon);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_prefs) {
            mListener.switchFragment(SECTION_PREFERENCES);
        }
        return super.onOptionsItemSelected(item);
    }

    /*
     * ViewからFragment内のボタンの状態を設定
     */
    @Override
    public void setButtonEnabled(boolean enabled) {
        startButton.setEnabled(enabled);
        startButton.invalidate();
    }
}
