package jp.or.ixqsware.robotcarapp.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.or.ixqsware.robotcarapp.R;
import jp.or.ixqsware.robotcarapp.parts.ImageRecognition;
import jp.or.ixqsware.robotcarapp.parts.MapInfo;

import static jp.or.ixqsware.robotcarapp.Constants.ARG_SECTION_NUMBER;
import static jp.or.ixqsware.robotcarapp.Constants.SECTION_MAP;

public class CameraFragment extends BaseFragment {
    private Activity parentActivity;
    private TextureView textureView;
    private ImageView imageView;
    private ImageButton captureButton;
    private ImageButton okButton;
    private ImageButton cancelButton;
    private Size previewSize;
    private ImageReader imageReader;
    private Handler backgroundHandler;
    private Handler imageViewHandler;
    private CameraDevice cameraDevice;
    private CaptureRequest.Builder captureRequestBuilder;
    private CameraCaptureSession captureSession;
    private ImageRecognition imageRecognition;

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 270);    //  90
        ORIENTATIONS.append(Surface.ROTATION_90, 0);     //   0
        ORIENTATIONS.append(Surface.ROTATION_180, 90);   // 270
        ORIENTATIONS.append(Surface.ROTATION_270, 180);  // 180
    }

    private TextureView.SurfaceTextureListener surfaceTextureListener
            = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            try {
                prepareCameraView();
            } catch (CameraAccessException e) {
                Toast.makeText(parentActivity, getString(R.string.error_text), Toast.LENGTH_SHORT)
                        .show();
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };

    private ImageReader.OnImageAvailableListener imageAvailableListener
            = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            try {
                captureSession.stopRepeating();
            } catch (CameraAccessException e) {
                createCameraCaptureSession();
                return;
            }

            Image image = reader.acquireNextImage();
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);

            // 画像認識
            imageRecognition.recognitionImage(bytes);
            Bitmap bmp = imageRecognition.getImage();
            if (bmp == null) { return; }
            image.close();

            // imageViewに認識結果を表示
            setImageToView(bmp);
        }
    };

    public static CameraFragment newInstance(int sectionNumber) {
        CameraFragment instance = new CameraFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        instance.setArguments(args);
        return instance;
    }

    public CameraFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_camera, container, false);

        textureView = rootView.findViewById(R.id.texture_view);
        textureView.setSurfaceTextureListener(surfaceTextureListener);

        captureButton = rootView.findViewById(R.id.capture_button);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        imageRecognition = ImageRecognition.getInstance(getContext());

        imageView = rootView.findViewById(R.id.image_view);
        imageView.setVisibility(View.GONE);

        okButton = rootView.findViewById(R.id.ok_button);
        cancelButton = rootView.findViewById(R.id.cancel_button);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MapFragmentに遷移
                mListener.transferData(SECTION_MAP,
                        new ArrayList<MapInfo>(){ {add(imageRecognition.getMapInfo());} });
            }
        });
        okButton.setVisibility(View.GONE);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 写真撮影に戻る
                imageView.setVisibility(View.GONE);
                okButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);

                textureView.setVisibility(View.VISIBLE);
                captureButton.setVisibility(View.VISIBLE);

                createCameraCaptureSession();
            }
        });
        cancelButton.setVisibility(View.GONE);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        HandlerThread thread = new HandlerThread("CameraPicture");
        thread.start();

        backgroundHandler = new Handler(thread.getLooper());
        imageViewHandler = new Handler();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parentActivity = (Activity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void takePicture() {
        if(null == cameraDevice) {
            return;
        }

        try {
            captureRequestBuilder.addTarget(imageReader.getSurface());
            captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

            // 画像の調整
            int rotation = getApplicationRotation();
            captureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            // キャプチャセッションの生成
            captureSession.capture(captureRequestBuilder.build(),
                    new CameraCaptureSession.CaptureCallback() {
                        @Override
                        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                                       @NonNull CaptureRequest request,
                                                       @NonNull TotalCaptureResult result) {
                            super.onCaptureCompleted(session, request, result);
                        }
                    },
                    backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void rotateTextureView() {
        int appOrientation = getApplicationRotation();
        int viewWidth = textureView.getWidth();
        int viewHeight = textureView.getHeight();

        Matrix matrix = new Matrix();
        matrix.postRotate(ORIENTATIONS.get(appOrientation), viewWidth * 0.5f, viewHeight * 0.5f);

        textureView.setTransform(matrix);
    }

    private int getApplicationRotation() {
        WindowManager windowManager
                = (WindowManager) parentActivity.getSystemService(Context.WINDOW_SERVICE);
        return windowManager.getDefaultDisplay().getRotation();
    }

    private Size getImageSize(CameraCharacteristics characteristics) {
        int width = 480;
        int height = 640;
        if (characteristics != null) {
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            Size maxSize = Collections.max(
                    Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                    new Comparator<Size>() {
                        @Override
                        public int compare(Size o1, Size o2) {
                            return Long.signum((long) o1.getWidth() * o1.getHeight() -
                                    (long) o2.getWidth() * o2.getHeight());
                        }
                    });

            width = maxSize.getWidth();
            height = maxSize.getHeight();
        }
        return new Size(width, height);
    }

    private void prepareCameraView() throws CameraAccessException {
        CameraManager cameraManager
                = (CameraManager) parentActivity.getSystemService(Context.CAMERA_SERVICE);

        String backCameraId = null;
        for (String id : cameraManager.getCameraIdList()) {
            CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(id);
            if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING)
                    == CameraMetadata.LENS_FACING_BACK) {
                backCameraId = id;
                StreamConfigurationMap streamConfigMap
                        = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (streamConfigMap == null) { continue; }

                previewSize = streamConfigMap.getOutputSizes(SurfaceTexture.class)[0];
                Size imageSize = getImageSize(cameraCharacteristics);

                imageReader = ImageReader.newInstance(
                        imageSize.getWidth(), imageSize.getHeight(),
                        ImageFormat.JPEG, 1);
                imageReader.setOnImageAvailableListener(imageAvailableListener, backgroundHandler);
                break;
            }
        }
        if (backCameraId == null) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(parentActivity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(parentActivity, getString(R.string.enable_camera), Toast.LENGTH_SHORT).show();
            return;
        }

        cameraManager.openCamera(backCameraId,
                new CameraDevice.StateCallback() {
                    @Override
                    public void onOpened(@NonNull CameraDevice camera) {
                        cameraDevice = camera;
                        createCameraCaptureSession();
                    }

                    @Override
                    public void onDisconnected(@NonNull CameraDevice camera) {
                    }

                    @Override
                    public void onError(@NonNull CameraDevice camera, int error) {
                    }
                }, null);
    }

    private void createCameraCaptureSession() {
        if (null == cameraDevice || !textureView.isAvailable() || null == previewSize) {
            return;
        }

        SurfaceTexture texture =  textureView.getSurfaceTexture();
        if (null == texture) { return; }

        // TextureViewを回転
        //rotateTextureView();

        // TODO バッファサイズを指定
        //texture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
        texture.setDefaultBufferSize(768, 1024);

        // 出力するためのTextureViewをセット
        Surface surface = new Surface(texture);
        List outputSurfaces = new ArrayList(2);
        outputSurfaces.add(imageReader.getSurface());
        outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));

        try {
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        captureRequestBuilder.addTarget(surface);

        try {
            cameraDevice.createCaptureSession(outputSurfaces,
                    new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    captureSession = cameraCaptureSession;
                    try {
                        updatePreview();
                    } catch (CameraAccessException e) {
                        Toast.makeText(parentActivity,
                                getString(R.string.error_text), Toast.LENGTH_SHORT)
                                .show();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(parentActivity, "onConfigureFailed", Toast.LENGTH_LONG).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void updatePreview() throws CameraAccessException {
        if (cameraDevice == null) { return; }
        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

        captureSession.stopRepeating();

        captureSession.setRepeatingRequest(captureRequestBuilder.build(),
                null, backgroundHandler);
    }

    private void setImageToView(final Bitmap bmp) {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                imageView.setImageDrawable(new BitmapDrawable(getResources(), bmp));
                imageView.setVisibility(View.VISIBLE);
                okButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                textureView.setVisibility(View.GONE);
                captureButton.setVisibility(View.GONE);
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                imageViewHandler.post(runnable);
            }
        }).start();
    }
}
