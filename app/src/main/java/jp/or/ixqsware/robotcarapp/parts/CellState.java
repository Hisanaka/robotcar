package jp.or.ixqsware.robotcarapp.parts;

/**
 * マップ上の1コマの状態を保持する。
 *
 * Created by hnakadate on 2017/06/04.
 */

public class CellState {
    public enum CELL_STATUS {
        ON,
        OFF
    }

    private CELL_STATUS cellStatus = CELL_STATUS.ON;

    private int top;
    private int left;
    private float width;
    private float height;
    private String object = "";

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    void setCellStatus(CELL_STATUS status_) {
        this.cellStatus = status_;
    }

    public CELL_STATUS getCellStatus() {
        return this.cellStatus;
    }

    void setTop(int top_) {
        this.top = top_;
    }

    public int getTop() {
        return this.top;
    }

    void setLeft(int left_) {
        this.left = left_;
    }

    public int getLeft() {
        return left;
    }

    void setWidth(float width_) {
        this.width = width_;
    }

    public float getWidth() {
        return width;
    }

    public float getCentreX() {
        return this.left + this.width / 2;
    }

    public float getCentreY() {
        return this.top + this.height / 2;
    }

    void setHeight(float height_) {
        this.height = height_;
    }

    public float getHeight() {
        return height;
    }
}
