package jp.or.ixqsware.robotcarapp.parts;

import java.util.ArrayList;

/**
 * 動作情報
 * Created by hnakadate on 2017/08/05.
 */

public class ActionInfo {
    private ArrayList<Integer> arrInfo;

    public ActionInfo() {
        arrInfo = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            arrInfo.add(i, 0);
        }
    }

    public ArrayList<Integer> getActionInfo() {
        return this.arrInfo;
    }

    public void setAction(int action) {
        this.arrInfo.set(0, action);
    }

    public void setPower1(int power) {
        this.arrInfo.set(1, power);
    }

    public void setPower2(int power) {
        this.arrInfo.set(2, power);
    }

    public void setTime(int time) {
        this.arrInfo.set(3, time);
    }
}
