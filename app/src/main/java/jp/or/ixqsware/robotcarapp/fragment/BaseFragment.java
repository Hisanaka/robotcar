package jp.or.ixqsware.robotcarapp.fragment;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

/**
 * 基本フラグメント
 *
 * Created by hnakadate on 2015/09/16.
 */
public class BaseFragment extends Fragment {
    protected FragmentCallbackListener mListener;

    public interface FragmentCallbackListener {
        void scanDevices();
        void connectToDevice(BluetoothDevice device);
        void disconnectDevice();
        void writeCharacteristic(String uuid, int mValue);
        void writeCharacteristic(String uuid, ArrayList<Integer> arrValues);
        void switchFragment(int sectionNumber);
        void transferData(int sectionNumber, ArrayList<? extends Parcelable> objects);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCallbackListener) {
            mListener = (FragmentCallbackListener) context;
        }
    }
}
