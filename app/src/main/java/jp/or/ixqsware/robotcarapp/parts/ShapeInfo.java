package jp.or.ixqsware.robotcarapp.parts;

import org.opencv.core.MatOfPoint;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ShapeInfo {
    private String shapeType;
    private List<MatOfPoint> listContours;
    private int cellNumber;

    public ShapeInfo() {
        super();
        shapeType = "";
        listContours = new ArrayList<>();
        cellNumber = -1;
    }

    public void setType(String type) {
        this.shapeType = type;
    }

    public String getShapeType() {
        return this.shapeType;
    }

    public void setContour(MatOfPoint point) {
        this.listContours.add(point);
    }

    public List<MatOfPoint> getContours() {
        return this.listContours;
    }

    public void setCellNumber(int number) {
        this.cellNumber = number;
    }

    public int getCellNumber() {
        return this.cellNumber;
    }
}
