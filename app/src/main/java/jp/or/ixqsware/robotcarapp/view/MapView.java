package jp.or.ixqsware.robotcarapp.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import jp.or.ixqsware.robotcarapp.R;
import jp.or.ixqsware.robotcarapp.parts.CellAddress;
import jp.or.ixqsware.robotcarapp.parts.CellState;
import jp.or.ixqsware.robotcarapp.parts.Map;
import jp.or.ixqsware.robotcarapp.parts.MapInfo;
import jp.or.ixqsware.robotcarapp.parts.ShapeInfo;

/**
 * マップビュー
 *
 * Created by hnakadate on 2017/06/03.
 */

public class MapView extends View {
    private Map map;
    private Paint paint = new Paint();
    private int curRow = -1;
    private int curCol = -1;
    private boolean onDrawing = false;
    private MapInfo mapInfo;
    private ViewCallbackListener mListener;

    public interface ViewCallbackListener {
        void setButtonEnabled(boolean enabled);
    }

    public void setListener(ViewCallbackListener listener) {
        this.mListener = listener;
    }

    public MapView(Context context) {
        super(context);
        map = new Map();
        setFocusable(true);
    }

    public MapView(Context context, MapInfo mapInfo_) {
        super(context);
        this.mapInfo = mapInfo_;
        if (this.mapInfo != null) {
            map = new Map(this.mapInfo.getRows(), this.mapInfo.getCols());
        } else {
            map = new Map();
        }
        setFocusable(true);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        map.setWidth(getWidth());
        map.setHeight(getHeight());
        drawMap(canvas);
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        int row = (int) (y / map.getCellHeight());
        int col = (int) (x / map.getCellWidth());
        if (map.getRowsCount() <= row || map.getColumnsCount() <= col) { return true; }

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_UP:
                if (row == 0 && col == map.getColumnsCount() - 1 && onDrawing) {
                    // 右上がゴール(ゴールにたどり着いたらStartボタンを有効にする)
                    onDrawing = false;
                    mListener.setButtonEnabled(true);
                }
                break;

            case MotionEvent.ACTION_DOWN:
                if (row == map.getRowsCount() - 1 && col == 0) {
                    // 左下からスタート
                    resetStatus();
                    map.toggleCellStatus(row, col, CellState.CELL_STATUS.ON);
                    onDrawing = true;
                    curRow = row;
                    curCol = col;
                } else if (row == 0 && col == map.getColumnsCount() - 1 && onDrawing) {
                    // ゴールにたどり着いている場合は処理しない
                    onDrawing = false;
                } else if (row == curRow && col == curCol && onDrawing) {
                    curRow = row;
                    curCol = col;
                    onDrawing = true;
                } else {
                    onDrawing = false;
                }
                invalidate();
                break;

            case MotionEvent.ACTION_MOVE:
                //if (!onDrawing) { return true; }
                if (curRow == row && curCol == col) { return true; }
                if (curRow != row && curCol != col) { return true; }  // 斜め移動は禁止
                if (Math.abs(curRow - row) > 1 || Math.abs(curCol - col) > 1) { return true; }

                int[] nextToLastCell = map.getNextToLastCellAddress();
                if (nextToLastCell[0] == row && nextToLastCell[1] == col) {
                    // 1つ前のセルに戻った場合は、最後のセルの選択を解除する
                    if (!map.isSelected(row, col)) {
                        map.toggleCellStatus(curRow, curCol, CellState.CELL_STATUS.OFF);
                        mListener.setButtonEnabled(false);
                        onDrawing = true;
                    }
                } else if (row == 0 && col == map.getColumnsCount() - 1) {
                    // 右上がゴール(ゴールにたどり着いたらStartボタンを有効にする)
                    map.toggleCellStatus(row, col, CellState.CELL_STATUS.ON);
                    mListener.setButtonEnabled(true);
                    onDrawing = false;
                } else if (map.getObject(row, col).equalsIgnoreCase("circle")
                        || map.getObject(row, col).equalsIgnoreCase("triangle")
                        || !onDrawing) {
                    // 特定の図形のあるセルには移動させない
                    return true;
                } else if (map.getCells()[row][col].getCellStatus() == CellState.CELL_STATUS.ON) {
                    // 一度通ったセルは通れない
                    return true;
                } else {
                    map.toggleCellStatus(row, col, CellState.CELL_STATUS.ON);
                    onDrawing = true;
                }
                curRow = row;
                curCol = col;
                invalidate();
                break;

            case MotionEvent.ACTION_CANCEL:
            default:
                break;
        }

        return true;
    }

    public ArrayList<Integer> getDirection() {
        return map.getDirections();
    }

    private void resetStatus() {
        for (int r = 0; r < map.getRowsCount(); r++) {
            for (int c = 0; c < map.getColumnsCount(); c++) {
                map.toggleCellStatus(r, c, CellState.CELL_STATUS.OFF);
            }
        }
        map.resetRoute();
        mListener.setButtonEnabled(false);
        curRow = -1;
        curCol = -1;
    }

    /*
     * 枠線の描画
     */
    private void drawBorders(Canvas canvas) {
        int cellWidth = map.getCellWidth();
        int cellHeight = map.getCellHeight();

        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(3);

        // 横線
        for (int r = 0; r < map.getRowsCount() + 1; r++) {
            canvas.drawLine(0, cellHeight * r,
                    cellWidth * map.getColumnsCount(), cellHeight * r, paint);
        }

        // 縦線
        for (int c = 0; c < map.getColumnsCount() + 1; c++) {
            canvas.drawLine(cellWidth * c, 0,
                    cellWidth * c, cellHeight * map.getRowsCount(), paint);
        }
    }

    /*
     * 物体の描画
     */
    private void drawObjects(Canvas canvas) {
        paint.setStyle(Paint.Style.FILL_AND_STROKE);

        CellState cells[][] = map.getCells();
        int cntRows = map.getRowsCount();
        int cntCols = map.getColumnsCount();
        List<ShapeInfo> shapeInfoList = this.mapInfo.getShapes();
        for (ShapeInfo shapeInfo : shapeInfoList) {
            String objType = shapeInfo.getShapeType();
            int cellNumber = shapeInfo.getCellNumber();
            if (cellNumber < 0) { continue; }

            int r = cellNumber % cntRows;
            int c = cellNumber / cntRows;
            cells[r][c].setObject(objType);

            float centerX = cells[r][c].getCentreX();
            float centerY = cells[r][c].getCentreY();
            double outerRadius = cells[r][c].getWidth() / 3;
            double innerRadius = cells[r][c].getWidth() / 6;

            switch (objType) {
                case "star":
                    drawPolygon(canvas, 5, centerX, centerY, outerRadius, innerRadius,
                            cells[r][c].getHeight(), Color.YELLOW);
                    break;

                case "circle":
                    paint.setColor(Color.BLACK);
                    canvas.drawCircle(centerX, centerY, (float) outerRadius, paint);
                    break;

                case "triangle":
                    drawPolygon(canvas, 3, centerX, centerY, outerRadius, innerRadius,
                            cells[r][c].getHeight(),
                            ContextCompat.getColor(getContext(), R.color.brown));
                    break;
            }
        }
    }

    private void drawPolygon(Canvas canvas, int points, float centerX, float centerY,
                            double outerRadius, double innerRadius, float adjHeight, int color) {
        Path path = new Path();
        path.reset();
        path.moveTo(centerX, centerY + adjHeight / 5);

        paint.setColor(color);
        for (int i = 0; i <= 2 * points; i++) {
            double angle = i * Math.PI/ points - Math.PI / 2;
            double radius = i % 2 == 0 ? outerRadius : innerRadius;
            path.lineTo((float) (centerX + radius * Math.cos(angle)),
                    (float) (centerY + radius * Math.sin(angle)));
        }
        canvas.drawPath(path, paint);
    }

    /*
     * 矢印の描画
     */
    private void drawArrows(Canvas canvas) {
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(8);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);

        CellState cells[][] = map.getCells();
        float cellWidth = map.getCellWidth();

        ArrayList<CellAddress> arrRoute = map.getRoute();
        for (int i = 1 ; i < arrRoute.size(); i++) {
            int[] pos1 = arrRoute.get(i - 1).getAddress();
            int[] pos2 = arrRoute.get(i).getAddress();

            CellState cell1 = cells[pos1[0]][pos1[1]];
            CellState cell2 = cells[pos2[0]][pos2[1]];

            // 矢印の描画
            float vX = cell2.getCentreX() - cell1.getCentreX();
            float vY = cell2.getCentreY() - cell1.getCentreY();
            double v = Math.sqrt(vX * vX + vY * vY);
            double uX = vX / v;
            double uY = vY / v;

            int w = (int) (cellWidth / 8);
            int h = (int) (w * Math.sqrt(3));
            int[] posL = new int[] {
                    (int) (cell2.getCentreX() - uY * w - uX * h),
                    (int) (cell2.getCentreY() + uX * w - uY * h)
            };
            int[] posR = new int[] {
                    (int) (cell2.getCentreX() + uY * w - uX * h),
                    (int) (cell2.getCentreY() - uX * w - uY * h)
            };

            Path path = new Path();
            path.moveTo(cell1.getCentreX(), cell1.getCentreY());
            path.lineTo(cell2.getCentreX(), cell2.getCentreY());
            path.lineTo(posL[0], posL[1]);
            path.lineTo(posR[0], posR[1]);
            path.lineTo(cell2.getCentreX(), cell2.getCentreY());

            canvas.drawPath(path, paint);
        }
    }

    /*
     * 選択したセルを塗りつぶす
     */
    private void fillInCells(Canvas canvas, int row, int col, int color) {
        paint.setColor(color);
        CellState cells[][] = map.getCells();
        canvas.drawRect(cells[row][col].getLeft(), cells[row][col].getTop(),
                cells[row][col].getLeft() + cells[row][col].getWidth(),
                cells[row][col].getTop() + cells[row][col].getHeight(), paint);
    }

    private void drawText(Canvas canvas, int row, int col, String text) {
        paint.setColor(Color.WHITE);
        paint.setTextSize(60);
        paint.setTextAlign(Paint.Align.CENTER);
        CellState cells[][] = map.getCells();
        canvas.drawText(text, cells[row][col].getCentreX(),
                cells[row][col].getCentreY() + 18, paint);
    }

    private void drawMap(Canvas canvas) {
        int mapWidth = map.getWidth();
        int mapHeight = map.getHeight();
        if (mapWidth == 0 || mapHeight == 0) { return; }

        drawBorders(canvas);

        fillInCells(canvas, map.getRowsCount() - 1, 0,
                ContextCompat.getColor(getContext(), R.color.color_start));
        fillInCells(canvas, 0, map.getColumnsCount() - 1,
                ContextCompat.getColor(getContext(), R.color.color_goal));

        drawText(canvas, map.getRowsCount() - 1, 0,
                getContext().getString(R.string.start_label));
        drawText(canvas, 0, map.getColumnsCount() -1,
                getContext().getString(R.string.goal_label));

        drawObjects(canvas);

        drawArrows(canvas);
    }
}
