package jp.or.ixqsware.robotcarapp.parts;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class MapInfo implements Parcelable {
    private int rows;
    private int cols;
    private List<ShapeInfo> objects;

    public MapInfo() {
        super();
        objects = new ArrayList<>();
        rows = 0;
        cols = 0;
    }

    public void setRows(int rows_) {
        this.rows = rows_;
    }

    public int getRows() {
        return this.rows;
    }

    public void setCols(int cols_) {
        this.cols = cols_;
    }

    public int getCols() {
        return this.cols;
    }

    public void addShapeInfo(ShapeInfo info) {
        this.objects.add(info);
    }

    public List<ShapeInfo> getShapes() {
        return this.objects;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.rows);
        dest.writeInt(this.cols);
        dest.writeList(this.objects);
    }

    protected MapInfo(Parcel in) {
        this.rows = in.readInt();
        this.cols = in.readInt();
        this.objects = new ArrayList<ShapeInfo>();
        in.readList(this.objects, ShapeInfo.class.getClassLoader());
    }

    public static final Parcelable.Creator<MapInfo> CREATOR = new Parcelable.Creator<MapInfo>() {
        @Override
        public MapInfo createFromParcel(Parcel source) {
            return new MapInfo(source);
        }

        @Override
        public MapInfo[] newArray(int size) {
            return new MapInfo[size];
        }
    };
}
