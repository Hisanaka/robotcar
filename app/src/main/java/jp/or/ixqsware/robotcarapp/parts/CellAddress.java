package jp.or.ixqsware.robotcarapp.parts;

/**
 * 選択した順番にセルを返す
 * Created by hnakadate on 2017/06/08.
 */
public class CellAddress {
    private int row;
    private int col;

    void setAddress(int row_, int col_) {
        this.row = row_;
        this.col = col_;
    }

    public int[] getAddress() {
        return new int[]{this.row, this.col};
    }
}
